# Python Dependency Graph
A tool to create diagrams of your python module.

## Usage
### Example usage
This script has been tested with [this project](https://codeberg.org/fabbecker/divera_python).
```shell
python -c "import dependency_graph as dg; import divera; module=divera.api; r=dg.analyse_module(module, depth=8); dg.write_to_json(r); dg.write_to_graph(r, title=module.__name__)"
```
