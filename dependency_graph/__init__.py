import inspect
import json
import os
import typing

import diagrams


def look_more_closely_at_this_file(
        path: str,
        f: str,
):
    return path.startswith(f)


def analyse_module(
        module,
        f: str = None,
        depth: int = -1,
        output: dict = None,
):
    """

    :param module:
    :param f: A path to a folder which contains all the analyzed modules. Everything that is not located in this folder
    will only be listed to
    :param depth:
    :param output:
    :return:
    """
    if output is None:
        output = {}
    if '__file__' not in dir(module):
        return
    if f and not look_more_closely_at_this_file(module.__file__, f):
        return
    if f is None:
        f = '/'.join(module.__file__.split('/')[:-2]) + '/'
    f = os.path.expanduser(f)
    if inspect.getfile(module) not in output:
        file = inspect.getfile(module)
        name = module.__name__
        if name not in output:
            output[name] = {
                'name': name,
                'groups': [],
                'parents': [],
            }
        for g in (file[len(f):] if file.startswith(f) else file).split('/'):
            if g not in output[name]['groups']:
                output[name]['groups'].append(g)
        for attr in dir(module):
            pm = module.__getattribute__(attr)
            pname = None
            groups = []
            parents = []
            if not inspect.ismodule(pm):
                if inspect.isfunction(pm) or inspect.isclass(pm):
                    pname = inspect.getmodule(pm).__name__ + '.' + pm.__name__
                    path = inspect.getfile(pm)
                    if look_more_closely_at_this_file(path, f):
                        for file in (path[len(f):] if path.startswith(f) else path).split('/'):
                            if file not in groups:
                                groups.append(file)
                        if inspect.isclass(pm):
                            for base in pm.__bases__:
                                base_name = inspect.getmodule(base).__name__ + '.' + base.__name__
                                try:
                                    base_file = inspect.getfile(base)
                                except TypeError:
                                    base_file = ''
                                if base_name not in parents:
                                    parents.append(base_name)
                                if look_more_closely_at_this_file(base_file, f):
                                    base_groups = (path[len(f):] if base_file.startswith(f) else path).split('/')
                                else:
                                    base_groups = []
                                if base_name not in output:
                                    output[base_name] = {
                                        'name': base_name,
                                        'groups': base_groups,
                                        'parents': [],
                                    }
                                    if depth != 0:
                                        analyse_module(
                                            base,
                                            output=output,
                                            f=f,
                                            depth=depth - 1,
                                        )
                else:
                    print('Not a module', pm)
                    continue
            if pname is None:
                pname = pm.__name__
            if pname not in output[name]['parents']:
                output[name]['parents'].append(pname)
            if pname not in output:
                output[pname] = {
                    'name': pname,
                    'groups': groups,
                    'parents': parents,
                }
                if depth != 0:
                    analyse_module(pm, output=output, f=f, depth=depth - 1)
    return output


def write_to_json(
        data,
        file: str = 'dependencies.json',
):
    with open(file, 'w') as f:
        f.write(
            json.dumps(
                data,
                indent=4,
            )
        )
        f.close()


def create_node(module) -> diagrams.Node:
    return diagrams.Node(
        label='\n'.join(module.split(r'.')),
    )


def create_nodes_in_cluster(
        cluster_names: typing.List[str],
        modules: [typing.List[dict], typing.Iterable[dict]],
        level=0,
        nodes: dict = None,
) -> dict[str, diagrams.Node]:
    modules = [m for m in modules]
    if nodes is None:
        nodes = {}
    if level < len(cluster_names):
        name = cluster_names[level]
    else:
        name = ''

    with diagrams.Cluster(name):
        def is_next_level(m):
            return len(m['groups']) > level + 1
        next_level_modules = filter(
            is_next_level,
            modules,
        )
        if len(cluster_names) > level:
            create_nodes_in_cluster(
                cluster_names=cluster_names.copy(),
                modules=next_level_modules,
                level=level + 1,
                nodes=nodes,
            )

        for module in filter(
                lambda m: not is_next_level(m),
                modules,
        ):
            nodes[module['name']] = create_node(module['name'])
    return nodes


def write_to_graph(
        data: dict,
        title='Module',
):
    with diagrams.Diagram(title, filename=title.replace(' ', '_'), direction="BT", show=False):
        nodes = {}

        def is_grouped(m):
            return 'groups' in m and m['groups']
        grouped_modules = [
            x
            for x in filter(
                is_grouped,
                data.values(),
            )
        ]
        groups = []
        for module in grouped_modules:
            if module['groups'] not in groups:
                groups.append(module['groups'])

        for g in groups:
            create_nodes_in_cluster(
                g,
                modules=filter(
                    lambda m: m['groups'] == g,
                    grouped_modules,
                ),
                nodes=nodes,
            )

        for module in filter(
            lambda m: not is_grouped(m),
            data.values()
        ):
            nodes[module['name']] = create_node(module['name'])

        for name in nodes:
            if 'parents' in data[name]:
                nodes[name] << [
                    nodes[parent]
                    for parent in data[name]['parents']
                    if parent in nodes
                ]
